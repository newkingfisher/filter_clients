# frozen_string_literal: true

require 'csv'
require 'json'

require_relative '../lib/client_filter'

RSpec.describe ClientFilter do
  it ' has_blank_field? return true if all are emtpy' do
    row = CSV::Row.new(['Email', 'First Name', 'Last Name', 'Residential Address Street', 'Residential Address Locality', 'Residential Address State', 'Residential Address Postcode', 'Postal Address Street', 'Postal Address Locality', 'Postal Address State', 'Postal Address Postcode'],
                       ['', '', '', '', '', '', '', '', '', '', ''])
    expect(ClientFilter.has_blank_field?(row)).to be true
  end

  it ' has_blank_field? return true if Email is empty' do
    row = CSV::Row.new(['Email', 'First Name', 'Last Name', 'Residential Address Street', 'Residential Address Locality', 'Residential Address State', 'Residential Address Postcode', 'Postal Address Street', 'Postal Address Locality', 'Postal Address State', 'Postal Address Postcode'],
                       ['', 'Darcy', 'Waters', '8540 Charli Summit', 'AIRLIE BEACH', 'QLD', '4802', '376 Williamson Hill',
                        'ARTHUR RIVER', 'WA', '6315'])
    expect(ClientFilter.has_blank_field?(row)).to be true
  end

  it ' has_blank_field? return true if First Name is empty' do
    row = CSV::Row.new(['Email', 'First Name', 'Last Name', 'Residential Address Street', 'Residential Address Locality', 'Residential Address State', 'Residential Address Postcode', 'Postal Address Street', 'Postal Address Locality', 'Postal Address State', 'Postal Address Postcode'],
                       ['xxx@gmail.com', '', 'Waters', '8540 Charli Summit', 'AIRLIE BEACH', 'QLD', '4802', '376 Williamson Hill',
                        'ARTHUR RIVER', 'WA', '6315'])
    expect(ClientFilter.has_blank_field?(row)).to be true
  end

  it '.filter will be not be selected if residential not matched postcode' do
    wrong_code = '9999'

    row = CSV::Row.new(['Email', 'First Name', 'Last Name', 'Residential Address Street', 'Residential Address Locality', 'Residential Address State', 'Residential Address Postcode', 'Postal Address Street', 'Postal Address Locality', 'Postal Address State', 'Postal Address Postcode'],
                       ['leslie@qq.com', 'Darcy', 'Waters', '8540 Charli Summit', 'AIRLIE BEACH', 'QLD', wrong_code,
                        '376 Williamson Hill', 'ARTHUR RIVER', 'WA', '6315'])
    expect(ClientFilter.filter([row])).to eq([])
  end

  it '.filter will be not be selected if postal location not matched postcode' do
    wrong_postal_code = '9999'

    row = CSV::Row.new(['Email', 'First Name', 'Last Name', 'Residential Address Street', 'Residential Address Locality', 'Residential Address State', 'Residential Address Postcode', 'Postal Address Street', 'Postal Address Locality', 'Postal Address State', 'Postal Address Postcode'],
                       ['leslie@qq.com', 'Darcy', 'Waters', '8540 Charli Summit', 'AIRLIE BEACH', 'QLD', '4802', '376 Williamson Hill',
                        'ARTHUR RIVER', 'WA', wrong_postal_code])
    expect(ClientFilter.filter([row]).size).to be(0)
  end

  it 'will filter any valid clients' do
    row = CSV::Row.new(['Email', 'First Name', 'Last Name', 'Residential Address Street', 'Residential Address Locality', 'Residential Address State', 'Residential Address Postcode', 'Postal Address Street', 'Postal Address Locality', 'Postal Address State', 'Postal Address Postcode'],
                       ['leslie@qq.com', 'Darcy', 'Waters', '8540 Charli Summit', 'AIRLIE BEACH', 'QLD', '4802', '376 Williamson Hill',
                        'ARTHUR RIVER', 'WA', '6315'])
    expect(ClientFilter.filter([row]).size).to be(1)
  end

  it 'will filter one valid clients, and reject invalid one ' do
    valid = CSV::Row.new(['Email', 'First Name', 'Last Name', 'Residential Address Street', 'Residential Address Locality', 'Residential Address State', 'Residential Address Postcode', 'Postal Address Street', 'Postal Address Locality', 'Postal Address State', 'Postal Address Postcode'],
                         ['leslie@qq.com', 'Darcy', 'Waters', '8540 Charli Summit', 'AIRLIE BEACH', 'QLD', '4802', '376 Williamson Hill',
                          'ARTHUR RIVER', 'WA', '6315'])
    invalid = CSV::Row.new(['Email', 'First Name', 'Last Name', 'Residential Address Street', 'Residential Address Locality', 'Residential Address State', 'Residential Address Postcode', 'Postal Address Street', 'Postal Address Locality', 'Postal Address State', 'Postal Address Postcode'],
                           ['leslie@qq.com', 'Darcy', 'Waters', '8540 Charli Summit', 'AIRLIE BEACH', 'QLD', '4802', '376 Williamson Hill',
                            'ARTHUR RIVER', 'WA', 'wrong_code'])

    expect(ClientFilter.filter([valid, invalid]).size).to be(1)
  end

  it 'filter and valid rows have Resid_latitude, Resid_longitude, Postal_latitude, Postal_longitude ' do
    valid = CSV::Row.new(['Email', 'First Name', 'Last Name', 'Residential Address Street', 'Residential Address Locality', 'Residential Address State', 'Residential Address Postcode', 'Postal Address Street', 'Postal Address Locality', 'Postal Address State', 'Postal Address Postcode'],
                         ['leslie@qq.com', 'Darcy', 'Waters', '8540 Charli Summit', 'AIRLIE BEACH', 'QLD', '4802', '376 Williamson Hill',
                          'ARTHUR RIVER', 'WA', '6315'])
    geo = %w[Resid_latitude Resid_longitude Postal_latitude Postal_longitude]                         
    expect(ClientFilter.filter([valid]).first.headers).to include(*geo)
  end


end
