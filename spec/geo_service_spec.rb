# frozen_string_literal: true

require_relative '../lib/geo_service'

RSpec.describe GeoService do
  it 'should load the geo_db.json file' do
    expect(subject.geo_data).to be_a(Hash)
  end

  it 'returns empty when postcode is nil' do
    expect(subject.fetch_by_postcode(nil)).to eq([])
  end

  it 'returns empty when postcode is empty' do
    expect(subject.fetch_by_postcode('')).to eq([])
  end

  it 'returns empty when postcode is XXXX' do
    expect(subject.fetch_by_postcode('xxxx')).to eq([])
  end

  it 'returns empty when postcode is 123' do
    expect(subject.fetch_by_postcode('123')).to eq([])
  end

  it 'returns empty when postcode is 2464' do
    expect(subject.fetch_by_postcode('2464').map { |e| e['locality'] }).to include('FREEBURN ISLAND')
  end
end
