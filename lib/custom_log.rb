# frozen_string_literal: true

require 'logger'

class CustomLog
  def self.log
    if @logger.nil?
      @logger = Logger.new('/tmp/custom_log.log')
      @logger.level = Logger::INFO
      @logger.datetime_format = '%Y-%m-%d %H:%M:%S '
    end
    @logger
  end
end
