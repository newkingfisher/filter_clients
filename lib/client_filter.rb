# frozen_string_literal: true
# frozen_string_literal: tru

require 'pry'

require_relative './geo_service'

# Email
# First Name
# Last Name
# Residential Address Street
# Residential Address Locality
# Residential Address State
# Residential Address Postcode
# Postal Address Street
# Postal Address Locality
# Postal Address State
# Postal Address Postcode

module ClientFilter
  module_function

  def has_blank_field?(row)
    fields = row.headers.reject { |e| e.include? 'Postcode' }
    row.fields(*fields).any? { |e| e.nil? || e.empty? }
  end

  def filter(rows)
    postcodes = rows.map { |e| [e['Residential Address Postcode'], e['Postal Address Postcode']] }
                    .flatten.compact
                    .filter { |e| !e.nil? && e =~ /^[0-9]{4}$/ }
                    .uniq

    @geo_data ||= prepare_geo_data(postcodes)
    # binding.pry
    rows.select { |row| match?(row, @geo_data) }.each do |row|
      info = find_residential_matched(row, @geo_data)
      # p "info is ", info
      row['Resid_latitude'] = info['latitude']
      row['Resid_longitude'] = info['longitude']

      postal_info = find_postal_matched(row, @geo_data)

      row['Postal_latitude'] = postal_info['latitude']
      row['Postal_longitude'] = postal_info['longitude']
    end
  end

  def prepare_geo_data(postcodes)
    @geo_data ||= GeoService.call(postcodes)
  end
  
  def clean_cache
    GeoService.clean_cache()
  end

  def match?(row, geo_data)
    residential_matched = geo_data.fetch(row['Residential Address Postcode'], []).any? do |e|
      match_residential(row, e)
    end

    postal_matched = geo_data.fetch(row['Postal Address Postcode'], []).any? do |e|
      match_postal(row, e)
    end

    residential_matched && postal_matched
  end

  def find_residential_matched(row, geo_data)
    residential_match = geo_data.fetch(row['Residential Address Postcode'], []).find do |e|
      match_residential(row, e)
    end
  end

  def find_postal_matched(row, geo_data)
    postal_matched = geo_data.fetch(row['Postal Address Postcode'], []).find do |e|
      match_postal(row, e)
    end
  end

  def match_postal(row, info)
    info['locality'] == row['Postal Address Locality'] &&
      info['state_abbreviation'] == row['Postal Address State']
  end

  def match_residential(row, info)
    info['locality'] == row['Residential Address Locality'] &&
      info['state_abbreviation'] == row['Residential Address State']
  end
end
