# frozen_string_literal: true

require 'uri'
require 'net/http'
require 'json'
require 'pry'
require 'fileutils'

require_relative './custom_log'

class GeoService
  attr_accessor :geo_data

  BASE_URL = 'http://api.jsacreative.com.au/v1/suburbs'

  def initialize
    load
  end

  def load(file = File.expand_path('../geo_db.json', __dir__))
    CustomLog.log.info("GeoService: load from #{file}")
    @geo_data ||= {}
    if File.exist?(file)
      File.open(file, 'r') do |f|
        @geo_data = JSON.load(f)
      end
    end
  end

  def self.clean_cache(file = File.expand_path('../geo_db.json', __dir__))
    FileUtils.rm(file) if File.exist?(file)
  end

  def self.call(postcodes)
    service = GeoService.new
    need_to_fetch = postcodes - (service.geo_data&.keys || [])

    thread_size = 4
    Thread.abort_on_exception = true
    @queue = need_to_fetch.inject(Queue.new, :push)
    threads = Array.new(thread_size) do
      Thread.new do
        until @queue.empty?
          next_code = @queue.shift
          res = service.fetch_by_postcode(next_code)
          service.geo_data[next_code] = res
        end
      end
    end
    threads.each(&:join)
    service.save
    service.geo_data
  end

  def self.call_one(postcode)
    service = GeoService.new
    service.fetch_by_postcode(postcode)
  end

  def save(file = File.expand_path('../geo_db.json', __dir__))
    File.open(file, 'w') do |f|
      f.write(JSON.pretty_generate(geo_data))
    end
  end

  def fetch_by_postcode(postcode)
    return [] if postcode.nil? || postcode.empty? || postcode !~ /^[0-9]{4}$/

    retries ||= 0
    # TODO: should consider adding some other ways to get the geo data in case of this api exception
    begin
      url = URI("#{BASE_URL}?postcode=#{postcode}")
      http = Net::HTTP.new(url.host, url.port)
      request = Net::HTTP::Get.new(url)
      request['Content-Type'] = 'application/x-www-form-urlencoded'
      response = http.request(request)
      CustomLog.log.info("GeoService: #{postcode} #{response.code}, response: #{response.body}")
      JSON(response.read_body)
    rescue SocketError, Timeout::Error => e
      CustomLog.log.error("Timeout error: #{e}")
      retry if (retries += 1) < 3
    rescue StandardError => e
      CustomLog.log.error("Error: get postcode #{postcode} error,  #{e}")
      []
    end
  end
end
